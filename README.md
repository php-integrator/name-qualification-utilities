# Serenata - Name Qualification Utilities
Provides various utilities related to name qualification in PHP:

* Determine what namespace and imports are active at a specific location in a file
* Resolve names to their fully qualified name based on use statements and namespaces
* Localize names from a fully qualified name back to a local name based on use statements and namespaces

This project falls under the [Serenata](https://gitlab.com/Serenata) umbrella.

## Installation
```sh
composer require "serenata/name-qualification-utilities"
```

## Usage
```php
<?php

use Serenata\Common\Position;
use Serenata\Common\FilePosition;

use Serenata\NameQualificationUtilities;

// NOTE: Implement a suitable provider in your code.
$fileNamespaceProvider = new class implements NameQualificationUtilities\FileNamespaceProviderInterface {
    public function provide(string $file): array
    {
        return $arrayOfNamespaceObjectsWithImportsAttached;
    }
};

$positionalNamespaceDeterminer = new NameQualificationUtilities\PositionalNamespaceDeterminer($fileNamespaceProvider);

$factory = new NameQualificationUtilities\PositionalNameResolverFactory($positionalNamespaceDeterminer);

// Create a name resolver that can resolve names using the imports and namespace that apply at the specified position.
$position = new Position($someLineNumber, $someCharacterIndexOnLine);
$filePosition = new FilePosition('MyPhpFile.php', $position);

$nameResolver = $factory->create($filePosition);

$result = $nameResolver->resolve('A', NameQualificationUtilities\NameKind::CLASSLIKE);

// $result will now contain the FQCN of the class, e.g. '\A', '\SomeNamespace\A', ...
```

## Not a PHP parser
Actual parsing of code to find available imports and namespaces is not provided, but you can use a tool such as [php-parser](https://github.com/nikic/PHP-Parser) for this. Instead, the available imports and namespaces have to be provided by the user of this library by implementing the interface `FileNamespaceProviderInterface`.

## What is the "kind" parameter all about?
It is necessary because PHP supports use statements or imports with the same name, as long as their "kind" is different. In other words, you can import a constant with the same name as a class:

```php
<?php

use A\B;
use function A\B;
use const A\B;
```

## What is the difference between `NamespaceNameResolver` and `StructureAwareNameResolver`?
Some constant and function names can be ambiguous, for example:

```php
<?php

namespace A;

$a = SOME_CONSTANT;
```

`SOME_CONSTANT` can refer to `\A\SOME_CONSTANT` or `\SOME_CONSTANT`, PHP determines which one to use at runtime:

1. Resolve to `\A\SOME_CONSTANT` if it exists
2. Resolve to `\SOME_CONSTANT` if it exists
3. Resolve to `\A\SOME_CONSTANT` again and fail because neither exist

`NamespaceNameResolver` does not know how to handle this situation and will bail out with an exception that the name is ambiguous.

`StructureAwareNameResolver` will request additional information - from a provider you must inject into it - to determine which structures actually exist in order to successfully resolve the name.

![GPLv3 Logo](https://gitlab.com/Serenata/Serenata/raw/793c93b0f69a5f4ba183f1dfff79f0c68d9bd010/resources/images/gpl_v3.png)
