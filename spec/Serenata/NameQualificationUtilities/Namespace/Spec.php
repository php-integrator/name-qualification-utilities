<?php

namespace spec\Serenata\NameQualificationUtilities;

use Serenata\Common\Range;
use Serenata\Common\Position;

use Serenata\NameQualificationUtilities\Import;
use Serenata\NameQualificationUtilities\NameKind;

use Serenata\NameQualificationUtilities\Exception\MalformedNameEncounteredException;

use PhpSpec\ObjectBehavior;

class Namespace_Spec extends ObjectBehavior
{
    /**
     * @return void
     */
    public function it_returns_configured_name(): void
    {
        $this->beConstructedWith('Test', [], new Range(new Position(0, 0), new Position(0, 0)));

        $this->getName()->shouldBe('Test');
    }

    /**
     * @return void
     */
    public function it_returns_configured_imports(): void
    {
        $import = new Import('Test', 'Test', NameKind::CLASSLIKE, new Position(0, 0));

        $this->beConstructedWith(null, [$import], new Range(new Position(0, 0), new Position(0, 0)));

        $this->getImports()->shouldBe([$import]);
    }

    /**
     * @return void
     */
    public function it_returns_configured_range(): void
    {
        $range = new Range(new Position(0, 0), new Position(0, 0));

        $this->beConstructedWith(null, [], $range);

        $this->getRange()->shouldBe($range);
    }

    /**
     * @return void
     */
    public function it_selects_the_appropriate_import_when_finding_imports_by_alias(): void
    {
        $items = [
            new Import('B\C\D', 'D', NameKind::CLASSLIKE, new Position(0, 0)),
            new Import('B\C\D', 'D', NameKind::FUNCTION_, new Position(0, 0)),
            new Import('B\C\D', 'D', NameKind::CONSTANT, new Position(0, 0))
        ];

        $this->beConstructedWith(null, $items, new Range(new Position(0, 0), new Position(0, 0)));

        $this->findImportForAlias('D', NameKind::CLASSLIKE)->shouldBe($items[0]);
        $this->findImportForAlias('D', NameKind::FUNCTION_)->shouldBe($items[1]);
        $this->findImportForAlias('D', NameKind::CONSTANT)->shouldBe($items[2]);
    }

    /**
     * @return void
     */
    public function it_returns_null_when_no_import_with_specified_alias_exists(): void
    {
        $this->beConstructedWith(null, [], new Range(new Position(0, 0), new Position(0, 0)));

        $this->findImportForAlias('D')->shouldBe(null);
    }

    /**
     * @return void
     */
    public function it_returns_null_as_fqcn_when_there_is_no_name(): void
    {
        $this->beConstructedWith(null, [], new Range(new Position(0, 0), new Position(0, 0)));

        $this->getFullyQualifiedName()->shouldBe(null);
    }

    /**
     * @return void
     */
    public function it_returns_name_as_is_when_it_is_fully_qualified(): void
    {
        $this->beConstructedWith('\D', [], new Range(new Position(0, 0), new Position(0, 0)));

        $this->getFullyQualifiedName()->shouldBe('\D');
    }

    /**
     * @return void
     */
    public function it_adds_leading_slash_when_name_is_not_fully_qualified(): void
    {
        $this->beConstructedWith('D', [], new Range(new Position(0, 0), new Position(0, 0)));

        $this->getFullyQualifiedName()->shouldBe('\D');
    }

    /**
     * @return void
     */
    public function it_throws_exception_for_empty_names(): void
    {
        $this->beConstructedWith('', [], new Range(new Position(0, 0), new Position(0, 0)));

        $this->shouldThrow(MalformedNameEncounteredException::class)->duringInstantiation();
    }
}
