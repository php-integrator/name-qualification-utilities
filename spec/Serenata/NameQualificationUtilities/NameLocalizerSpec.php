<?php

namespace spec\Serenata\NameQualificationUtilities;

use ArrayIterator;

use Serenata\Common\Range;
use Serenata\Common\Position;

use Serenata\NameQualificationUtilities\Import;
use Serenata\NameQualificationUtilities\NameKind;
use Serenata\NameQualificationUtilities\Namespace_;

use Serenata\NameQualificationUtilities\Exception\MalformedNameEncounteredException;

use PhpSpec\ObjectBehavior;

class NameLocalizerSpec extends ObjectBehavior
{
    /**
     * @var Range
     */
    private $dummyRange;

    /**
     * @var Position
     */
    private $dummyPosition;

    /**
     * @return void
     */
    public function let()
    {
        $this->dummyRange = new Range(
            new Position(0, 0),
            new Position(1, 0)
        );

        $this->dummyPosition = new Position(0, 1);
    }

    /**
     * @return void
     */
    public function it_ignores_non_fully_qualified_names(): void
    {
        $this->beConstructedWith(new Namespace_(null, [], $this->dummyRange));

        $this->localize('A\B')->shouldBe('A\B');
    }

    /**
     * @return void
     */
    public function it_returns_qualified_name_when_localization_is_impossible_and_not_in_namespace(): void
    {
        $this->beConstructedWith(new Namespace_(null, [], $this->dummyRange));

        $this->localize('\A\B')->shouldBe('A\B');
    }

    /**
     * @return void
     */
    public function it_returns_fully_qualified_name_when_localization_is_impossible_and_in_namespace(): void
    {
        $this->beConstructedWith(new Namespace_('A', [], $this->dummyRange));

        $this->localize('\SplFileInfo')->shouldBe('\SplFileInfo');
    }

    /**
     * @return void
     */
    public function it_makes_name_inside_subnamespace_relative_to_namespace(): void
    {
        $this->beConstructedWith(new Namespace_('A', [], $this->dummyRange));

        $this->localize('\A\B')->shouldBe('B');
    }

    /**
     * @return void
     */
    public function it_makes_name_relative_to_import(): void
    {
        $items = [
            new Import('A\B', 'B', NameKind::CLASSLIKE, $this->dummyPosition)
        ];

        $this->beConstructedWith(new Namespace_('N', $items, $this->dummyRange));

        $this->localize('\A\B')->shouldBe('B');
    }

    /**
     * @return void
     */
    public function it_makes_name_relative_to_aliased_import(): void
    {
        $items = [
            new Import('A\B', 'Alias', NameKind::CLASSLIKE, $this->dummyPosition)
        ];

        $this->beConstructedWith(new Namespace_('N', $items, $this->dummyRange));

        $this->localize('\A\B')->shouldBe('Alias');
    }

    /**
     * @return void
     */
    public function it_selects_best_match_when_multiple_possibilities_exist(): void
    {
        $items = [
            new Import('A\B', 'B', NameKind::CLASSLIKE, $this->dummyPosition),
            new Import('A\B\C', 'C', NameKind::CLASSLIKE, $this->dummyPosition)
        ];

        $this->beConstructedWith(new Namespace_('A', $items, $this->dummyRange));

        $this->localize('\A\B\C')->shouldBe('C');
    }

    /**
     * @return void
     */
    public function it_filters_imports_by_kind_during_resolving(): void
    {
        $items = [
            new Import('A\name', 'nameA', NameKind::CLASSLIKE, $this->dummyPosition),
            new Import('B\name', 'nameB', NameKind::FUNCTION_, $this->dummyPosition),
            new Import('C\name', 'nameC', NameKind::CONSTANT, $this->dummyPosition)
        ];

        $this->beConstructedWith(new Namespace_('N', $items, $this->dummyRange));

        $this->localize('\A\name', NameKind::CLASSLIKE)->shouldBe('nameA');
        $this->localize('\B\name', NameKind::FUNCTION_)->shouldBe('nameB');
        $this->localize('\C\name', NameKind::CONSTANT)->shouldBe('nameC');
    }

    /**
     * @return void
     */
    public function it_does_nothing_with_non_fully_qualified_names(): void
    {
        $this->beConstructedWith(new Namespace_(null, [], $this->dummyRange));

        $this->localize('C')->shouldBe('C');
    }

    /**
     * @return void
     */
    public function it_does_not_fail_on_zero(): void
    {
        $this->beConstructedWith(new Namespace_(null, [], $this->dummyRange));

        $this->localize('0')->shouldBe('0');
    }

    /**
     * @return void
     */
    public function it_throws_exception_for_empty_names(): void
    {
        $this->beConstructedWith(new Namespace_(null, [], $this->dummyRange));

        $this->shouldThrow(MalformedNameEncounteredException::class)->duringLocalize('', NameKind::CLASSLIKE);
    }
}
