<?php

namespace spec\Serenata\NameQualificationUtilities;

use Serenata\Common\Range;
use Serenata\Common\Position;
use Serenata\Common\FilePosition;

use Serenata\NameQualificationUtilities\Import;
use Serenata\NameQualificationUtilities\NameKind;
use Serenata\NameQualificationUtilities\Namespace_;
use Serenata\NameQualificationUtilities\FileNamespaceProviderInterface;

use PhpSpec\ObjectBehavior;

use Serenata\NameQualificationUtilities\PositionOutOfBoundsPositionalNamespaceDeterminerException;

class PositionalNamespaceDeterminerSpec extends ObjectBehavior
{
    /**
     * @var string
     */
    private const FILE = 'test.php';

    /**
     * @param FileNamespaceProviderInterface $fileNamespaceProviderInterface
     *
     * @return void
     */
    public function let(FileNamespaceProviderInterface $fileNamespaceProviderInterface): void
    {
        $namespaces = [
            new Namespace_(
                'A',
                [
                    new Import('N\B1', 'B1', NameKind::CLASSLIKE, new Position(3, 10)),
                    new Import('N\B2', 'B2', NameKind::CLASSLIKE, new Position(4, 10))
                ],
                new Range(
                    new Position(0, 5),
                    new Position(10, 1)
                )
            ),
            new Namespace_(
                null,
                [
                    new Import('N\B3', 'B3', NameKind::CLASSLIKE, new Position(13, 10)),
                    new Import('N\B4', 'B4', NameKind::CLASSLIKE, new Position(14, 10))
                ],
                new Range(
                    new Position(10, 1),
                    new Position(20, 1)
                )
            )
        ];

        $fileNamespaceProviderInterface->provide(self::FILE)->willReturn($namespaces);
    }

    /**
     * @param FileNamespaceProviderInterface $fileNamespaceProviderInterface
     *
     * @return void
     */
    public function it_selects_correct_namespace_for_position(FileNamespaceProviderInterface $fileNamespaceProviderInterface): void
    {
        $this->beConstructedWith($fileNamespaceProviderInterface);

        $position = new Position(10, 0);

        $limitedNamespace = new Namespace_(
            'A',
            [
                new Import('N\B1', 'B1', NameKind::CLASSLIKE, new Position(3, 10)),
                new Import('N\B2', 'B2', NameKind::CLASSLIKE, new Position(4, 10))
            ],
            new Range(
                new Position(0, 5),
                new Position(10, 1)
            )
        );

        $filePosition = new FilePosition(self::FILE, $position);

        $this->determine($filePosition)->shouldBeLike($limitedNamespace);
    }

    /**
     * @param FileNamespaceProviderInterface $fileNamespaceProviderInterface
     *
     * @return void
     */
    public function it_limits_imports_to_position(FileNamespaceProviderInterface $fileNamespaceProviderInterface): void
    {
        $this->beConstructedWith($fileNamespaceProviderInterface);

        $position = new Position(14, 0);

        $limitedNamespace = new Namespace_(
            null,
            [
                new Import('N\B3', 'B3', NameKind::CLASSLIKE, new Position(13, 10))
            ],
            new Range(
                new Position(10, 1),
                new Position(20, 1)
            )
        );

        $filePosition = new FilePosition(self::FILE, $position);

        $this->determine($filePosition)->shouldBeLike($limitedNamespace);
    }

    /**
     * @param FileNamespaceProviderInterface $fileNamespaceProviderInterface
     *
     * @return void
     */
    public function it_throws_exception_when_position_is_out_of_bounds(
        FileNamespaceProviderInterface $fileNamespaceProviderInterface
    ): void {
        $this->beConstructedWith($fileNamespaceProviderInterface);

        $position = new Position(21, 0);

        $limitedNamespace = new Namespace_(
            null,
            [
                new Import('N\B3', 'B3', NameKind::CLASSLIKE, new Position(13, 10))
            ],
            new Range(
                new Position(10, 1),
                new Position(20, 1)
            )
        );

        $filePosition = new FilePosition(self::FILE, $position);

        $this->shouldThrow(PositionOutOfBoundsPositionalNamespaceDeterminerException::class)->duringDetermine(
            $filePosition
        );
    }
}
