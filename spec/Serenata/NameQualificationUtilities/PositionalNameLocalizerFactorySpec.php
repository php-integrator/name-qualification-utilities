<?php

namespace spec\Serenata\NameQualificationUtilities;

use Serenata\Common\Range;
use Serenata\Common\Position;
use Serenata\Common\FilePosition;

use Serenata\NameQualificationUtilities\Namespace_;
use Serenata\NameQualificationUtilities\NameLocalizer;
use Serenata\NameQualificationUtilities\PositionalNamespaceDeterminerInterface;

use PhpSpec\ObjectBehavior;

class PositionalNameLocalizerFactorySpec extends ObjectBehavior
{
    /**
     * @param PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminerInterface
     *
     * @return void
     */
    public function it_creates_a_new_localizer(
        PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminerInterface
    ): void {
        $this->beConstructedWith($positionalNamespaceDeterminerInterface);

        $namespace = new Namespace_(null, [], new Range(new Position(0, 0), new Position(1, 1)));

        $position = new Position(1, 0);

        $filePosition = new FilePosition('test', $position);

        $positionalNamespaceDeterminerInterface->determine($filePosition)->willReturn($namespace);

        $resolver = new NameLocalizer($namespace);

        $this->create($filePosition)->shouldBeLike($resolver);
    }
}
