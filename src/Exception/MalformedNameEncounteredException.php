<?php

namespace Serenata\NameQualificationUtilities\Exception;

use RuntimeException;

/**
 * Exception that indicates resolving a name is immpossible because the name is malformed.
 */
class MalformedNameEncounteredException extends RuntimeException
{
}
