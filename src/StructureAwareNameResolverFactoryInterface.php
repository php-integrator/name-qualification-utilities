<?php

namespace Serenata\NameQualificationUtilities;

use Serenata\Common\FilePosition;

/**
 * Interface for factories that create instances of a {@see StructureAwareNameResolver}.
 */
interface StructureAwareNameResolverFactoryInterface
{
    /**
     * @param FilePosition $filePosition
     *
     * @return PositionalNameResolverInterface
     */
    public function create(FilePosition $filePosition): PositionalNameResolverInterface;
}
