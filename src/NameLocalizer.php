<?php

namespace Serenata\NameQualificationUtilities;

/**
 * Turns fully qualified names into their local equivalents.
 */
final class NameLocalizer implements NameLocalizerInterface
{
    /**
     * @var Namespace_
     */
    private $namespace;

    /**
     * @param Namespace_ $namespace
     */
    public function __construct(Namespace_ $namespace)
    {
        $this->namespace = $namespace;
    }

    /**
     * @inheritDoc
     */
    public function localize(string $name, string $kind = NameKind::CLASSLIKE): string
    {
        if ($name === '') {
            throw new Exception\MalformedNameEncounteredException('Name to localize can\'t be empty');
        } elseif ($name[0] !== '\\') {
            return $name;
        }

        $bestLocalizedName = $this->selectBestLocalizedNameFromImportList(
            $this->buildImportListWithNamespace($name),
            $name,
            $kind
        );

        if ($bestLocalizedName) {
            return $bestLocalizedName;
        } elseif ($this->namespace->getName() === null) {
            if ($name[0] === '\\') {
                return mb_substr($name, 1);
            }
        }

        return $name;
    }

    /**
     * @param string $name
     *
     * @return Import[]
     */
    protected function buildImportListWithNamespace(string $name): array
    {
        if ($this->namespace->getName() === null) {
            return $this->namespace->getImports();
        } elseif (mb_strpos($name, $this->namespace->getFullyQualifiedName()) !== 0) {
            return $this->namespace->getImports();
        }

        $nameWithoutNamespacePrefix = mb_substr($name, mb_strlen($this->namespace->getFullyQualifiedName()) + 1);
        $nameWithoutNamespacePrefixParts = explode('\\', $nameWithoutNamespacePrefix);

        // The namespace also acts as a use statement, but the rules are slightly different: in namespace A,  the
        // class \A\B becomes B rather than A\B (the latter which would happen if there were a use statement
        // "use A;").
        $imports = $this->namespace->getImports();
        $imports[] = new Import(
            $this->namespace->getFullyQualifiedName() . '\\' . $nameWithoutNamespacePrefixParts[0],
            $nameWithoutNamespacePrefixParts[0],
            NameKind::CLASSLIKE,
            $this->namespace->getRange()->getStart()
        );

        return $imports;
    }

    /**
     * @param Import[] $importList
     * @param string   $name
     * @param string   $kind
     *
     * @return string|null
     */
    protected function selectBestLocalizedNameFromImportList(
        array $importList,
        string $name,
        string $kind = NameKind::CLASSLIKE
    ): ?string {
        $bestLocalizedName = null;

        foreach ($importList as $import) {
            if ($import->getKind() !== $kind) {
                continue;
            } elseif (mb_strpos($name, $import->getFullyQualifiedName()) !== 0) {
                continue;
            }

            $localizedName = $import->getAlias() . mb_substr($name, mb_strlen($import->getFullyQualifiedName()));

            // It is possible that there are multiple use statements the FQCN could be made relative to (e.g.
            // if a namespace as well as one of its classes is imported), select the closest one in that case.
            if (!$bestLocalizedName || mb_strlen($localizedName) < mb_strlen($bestLocalizedName)) {
                $bestLocalizedName = $localizedName;
            }
        }

        return $bestLocalizedName;
    }
}
