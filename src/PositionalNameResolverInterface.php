<?php

namespace Serenata\NameQualificationUtilities;

use Serenata\Common\FilePosition;

/**
 * Interface for classes that can resolve local names to their fully qualified name based on their location.
 */
interface PositionalNameResolverInterface
{
    /**
     * @param string       $name         The name to resolve.
     * @param FilePosition $filePosition The position the type is located at.
     * @param string       $kind         Kind of type to resolve. A constant from {@see NameKind}.
     *
     * @throws Exception\MalformedNameEncounteredException
     * @throws Exception\UnresolvableNameEncounteredException
     *
     * @return string
     */
    public function resolve(string $name, FilePosition $filePosition, string $kind = NameKind::CLASSLIKE): string;
}
