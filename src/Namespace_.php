<?php

namespace Serenata\NameQualificationUtilities;

use UnexpectedValueException;

use Serenata\Common\Range;

/**
 * Represents data for a namespace.
 *
 * This is a value object and immutable.
 */
final class Namespace_
{
    /**
     * @var string|null
     */
    private $name;

    /**
     * @var Import[]
     */
    private $imports;

    /**
     * @var Range
     */
    private $range;

    /**
     * @param string|null $name
     * @param Import[]    $imports
     * @param Range       $range
     *
     * @throws Exception\MalformedNameEncounteredException
     */
    public function __construct(string $name = null, array $imports, Range $range)
    {
        if ($name === '') {
            throw new Exception\MalformedNameEncounteredException(
                'Name of namespace can\'t be an empty string, provide a valid name or pass null instead'
            );
        }

        $this->name = $name;
        $this->imports = $imports;
        $this->range = $range;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return Import[]
     */
    public function getImports(): array
    {
        return $this->imports;
    }

    /**
     * @return Range
     */
    public function getRange(): Range
    {
        return $this->range;
    }

    /**
     * @param string $alias
     * @param string $kind
     *
     * @return Import|null
     */
    public function findImportForAlias(string $alias, string $kind = NameKind::CLASSLIKE): ?Import
    {
        foreach ($this->getImports() as $import) {
            if ($import->getAlias() === $alias && $import->getKind() === $kind) {
                return $import;
            }
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function getFullyQualifiedName(): ?string
    {
        if ($this->getName() === null) {
            return null;
        } elseif ($this->getName()[0] !== '\\') {
            return '\\' . $this->getName();
        }

        return $this->getName();
    }
}
