<?php

namespace Serenata\NameQualificationUtilities;

use Serenata\Common\FilePosition;

/**
 * Interface for classes that create instances of {@see NameResolverInterface} that can resolve names at a specific
 * position in a file.
 */
interface PositionalNameResolverFactoryInterface
{
    /**
     * @param FilePosition $filePosition
     *
     * @return NameResolverInterface
     */
    public function create(FilePosition $filePosition): NameResolverInterface;
}
