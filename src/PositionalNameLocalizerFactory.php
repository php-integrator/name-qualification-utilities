<?php

namespace Serenata\NameQualificationUtilities;

use Serenata\Common\FilePosition;

/**
 * Creates instances of {@see NameLocalizerInterface} that can localize names at a specific position in a file.
 */
final class PositionalNameLocalizerFactory implements PositionalNameLocalizerFactoryInterface
{
    /**
     * @var PositionalNamespaceDeterminerInterface
     */
    private $positionalNamespaceDeterminer;

    /**
     * @param PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer
     */
    public function __construct(PositionalNamespaceDeterminerInterface $positionalNamespaceDeterminer)
    {
        $this->positionalNamespaceDeterminer = $positionalNamespaceDeterminer;
    }

    /**
     * @inheritDoc
     */
    public function create(FilePosition $filePosition): NameLocalizerInterface
    {
        return new NameLocalizer(
            $this->positionalNamespaceDeterminer->determine($filePosition)
        );
    }
}
