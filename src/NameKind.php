<?php

namespace Serenata\NameQualificationUtilities;

/**
 * Kinds of names.
 */
final class NameKind
{
    /**
     * @var string
     */
    public const CLASSLIKE = 'classlike';

    /**
     * @var string
     */
    public const FUNCTION_ = 'function';

    /**
     * @var string
     */
    public const CONSTANT  = 'constant';
}
