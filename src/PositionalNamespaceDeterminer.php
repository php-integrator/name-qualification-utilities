<?php

namespace Serenata\NameQualificationUtilities;

use Serenata\Common\Position;
use Serenata\Common\FilePosition;

/**
 * Determines which {@see Namespace_} is active at a specific location in a file.
 */
final class PositionalNamespaceDeterminer implements PositionalNamespaceDeterminerInterface
{
    /**
     * @var FileNamespaceProviderInterface
     */
    private $fileNamespaceProvider;

    /**
     * @param FileNamespaceProviderInterface $fileNamespaceProvider
     */
    public function __construct(FileNamespaceProviderInterface $fileNamespaceProvider)
    {
        $this->fileNamespaceProvider = $fileNamespaceProvider;
    }

    /**
     * @inheritDoc
     */
    public function determine(FilePosition $filePosition): Namespace_
    {
        $file = $filePosition->getFile();
        $position = $filePosition->getPosition();

        $namespaces = $this->fileNamespaceProvider->provide($file);

        foreach ($namespaces as $namespace) {
            if ($namespace->getRange()->contains($position)) {
                return $this->createCopyWithLimitedImportsFrom($namespace, $position);
            }
        }

        throw new PositionOutOfBoundsPositionalNamespaceDeterminerException(
            "Either position ({$position->getLine()}, {$position->getCharacter()}) is out of bounds in file " .
            "'{$file}', or no namespace was returned for it. Every position in file must match a single namespace " .
            "(files without explicit namespaces have an anonymous namespace)."
        );
    }

    /**
     * @param Namespace_ $namespace
     * @param Position   $position
     *
     * @return Namespace_
     */
    protected function createCopyWithLimitedImportsFrom(Namespace_ $namespace, Position $position): Namespace_
    {
        $imports = [];

        foreach ($namespace->getImports() as $import) {
            if ($position->liesAfterOrOn($import->getAppliesAfter())) {
                $imports[] = $import;
            }
        }

        return new Namespace_($namespace->getName(), $imports, $namespace->getRange());
    }
}
