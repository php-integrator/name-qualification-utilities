<?php

namespace Serenata\NameQualificationUtilities;

/**
 * Interface for classes that can indicate if a (global) constant exists or is present in a project or code base.
 */
interface ConstantPresenceIndicatorInterface
{
    /**
     * @param string $fullyQualifiedName
     *
     * @return bool
     */
    public function isPresent(string $fullyQualifiedName): bool;
}
