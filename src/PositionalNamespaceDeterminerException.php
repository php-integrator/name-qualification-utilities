<?php

namespace Serenata\NameQualificationUtilities;

use RuntimeException;

/**
 * Exception that can be thrown when positional namespace determination fails.
 */
class PositionalNamespaceDeterminerException extends RuntimeException
{

}
