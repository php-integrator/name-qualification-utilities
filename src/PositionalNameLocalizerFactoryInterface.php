<?php

namespace Serenata\NameQualificationUtilities;

use Serenata\Common\FilePosition;

/**
 * Interface for classes that create instances of {@see NameLocalizerInterface} that can localize names at a specific
 * position in a file.
 */
interface PositionalNameLocalizerFactoryInterface
{
    /**
     * @param FilePosition $filePosition
     *
     * @return NameLocalizerInterface
     */
    public function create(FilePosition $filePosition): NameLocalizerInterface;
}
