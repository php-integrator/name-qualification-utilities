<?php

namespace Serenata\NameQualificationUtilities;

/**
 * Interface for classes that turn fully qualified names into their local equivalents.
 */
interface NameLocalizerInterface
{
    /**
     * "Unresolves" a fully qualified name, turning it back into a name relative to local imports or the namespace.
     *
     * If no local name could be determined, the FQCN is returned, as that is the only way the name can be referenced
     * locally.
     *
     * @param string $name
     * @param string $kind
     *
     * @throws Exception\MalformedNameEncounteredException
     *
     * @example With use statement "use A\B as AliasB", unresolving "A\B\C\D" will yield "AliasB\C\D".
     *
     * @return string
     */
    public function localize(string $name, string $kind = NameKind::CLASSLIKE): string;
}
